#!/bin/bash

docker run -d \
-it \
--name=spectre \
-v "$(pwd)":/spectre \
sxscollaboration/spectrebuildenv:latest \
/bin/bash
