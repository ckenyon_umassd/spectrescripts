# SpectreScripts

## Description

This is a collection of scripts to be used to help compile the spectre code.

There are scripts for both docker and for singularity, these are helpful in
getting things started quicker.

It is worth noting that some of the options are preset with settings that work
best to avoid typos and explain some of the nuances of the commands (notably
docker -V).

---

## Files

- ***cmake\_script.sh***: runs the cmake command with particular options specified
- ***cmake\_singularity.sh***: Does the same as cmake\_script, but is adjusted to work
  with the intended path used in setting up the singularity build
- ***docker\_start.sh***: initializes a docker shell named "spectre"
- ***singularity\_build.sh***: builds spectre in a singularity shell
- ***singularity\_start.sh***: creates specter.img as a singularity shell

---
## Current State

Currently these work to compile 98% of the tests, with certain tests still
failing locally to my machine.
