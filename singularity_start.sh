#!/bin/bash

singularity build spectre.img docker://sxscollaboration/spectrebuildenv:latest

# Access this shell with the following command
# singularity shell spectre.img
